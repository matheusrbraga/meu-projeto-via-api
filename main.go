package main

import (
	"gitlab-api/repositoryFiles"
)

func main() {
	// authentication.ExemploBasicoAutenticacao()

	// application.CreateApplication()

	// impersonation.ExemploImpersonation()

	// labels.ExamploLabel()

	// languages.ExampleLanguage()

	// pagination.Paginacao()

	// personalAccessToken.PatRevokeExample()
	// personalAccessToken.PatListExampleWithUserFilter()
	// pipeline.Pipeline()

	// pipelineTestReport.PipelineTestReport()

	repositoryFiles.RepositoryFiles()

}
